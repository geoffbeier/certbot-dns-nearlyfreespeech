"""DNS Authenticator for NearlyFreeSpeech."""
import logging
import re

import zope.interface
from lexicon.providers import nfsn

from certbot import errors
from certbot import interfaces
from certbot.plugins import dns_common
from certbot.plugins import dns_common_lexicon

logger = logging.getLogger(__name__)


@zope.interface.implementer(interfaces.IAuthenticator)
@zope.interface.provider(interfaces.IPluginFactory)
class Authenticator(dns_common.DNSAuthenticator):
    """DNS Authenticator for NearlyFreeSpeech

    This Authenticator uses the NearlyFreeSpeech API to fulfill a dns-01 challenge.
    """

    description = 'Obtain certs using a DNS TXT record (if you are using NearlyFreeSpeech for DNS).'

    def __init__(self, *args, **kwargs):
        super(Authenticator, self).__init__(*args, **kwargs)
        self.credentials = None

    @classmethod
    def add_parser_arguments(cls, add):  # pylint: disable=arguments-differ
        super(Authenticator, cls).add_parser_arguments(add, default_propagation_seconds=20)
        add('credentials', help='NearlyFreeSpeech credentials INI file.')

    def more_info(self):  # pylint: disable=missing-docstring,no-self-use
        return 'This plugin configures a DNS TXT record to respond to a dns-01 challenge using ' + \
               'the NearlyFreeSpeech API.'

    def _setup_credentials(self):
        self.credentials = self._configure_credentials(
            'credentials',
            'NearlyFreeSpeech credentials INI file',
            {
                'key': 'API key for NearlyFreeSpeech account',
                'login': 'User name for NearlyFreeSpeech control panel',
            }
        )

    def _perform(self, domain, validation_name, validation):
        self._get_nearlyfreespeech_client().add_txt_record(domain, validation_name, validation)

    def _cleanup(self, domain, validation_name, validation):
        self._get_nearlyfreespeech_client().del_txt_record(domain, validation_name, validation)

    def _get_nearlyfreespeech_client(self):
        api_key = self.credentials.conf('key')
        api_user = self.credentials.conf('login')
        
        return _NearlyFreeSpeechLexiconClient(api_key, api_user)


class _NearlyFreeSpeechLexiconClient(dns_common_lexicon.LexiconClient):
    """
    Encapsulates all communication with the NearlyFreeSpeech API.
    """

    def __init__(self, api_key, api_user):
        super(_NearlyFreeSpeechLexiconClient, self).__init__()

        self.api_user = api_user
        self.api_key = api_key
        config = dns_common_lexicon.build_lexicon_config('nfsn', {}, {
            'auth_token' : api_key,
            'auth_username' : api_user,
        })
        self.provider = nfsn.Provider(config)

    def _handle_http_error(self, e, domain_name):
        # treat http error code 404 the same way _handle_general_error() treats 'No domain found'
        # so that the loop inside _find_domain_id() (the only place this method is called) can continue
        # iterating. Necessary because NFSN uses 404 for 'No domain found'
        if e.response.status_code == 404:
            return None
        return super._handle_http_error(self, e, domain_name)


    def _handle_general_error(self, e, domain_name):
        # Treat 404 as "no domain found" here as well now.
        if not str(e).startswith("404"):
            return super._handle_general_error(self, e, domain_name)
        return None
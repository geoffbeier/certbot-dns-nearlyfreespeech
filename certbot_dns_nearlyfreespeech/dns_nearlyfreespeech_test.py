"""Tests for certbot_dns_nearlyfreespeech.dns_nearlyfreespeech."""

import unittest

import mock

from certbot import errors
from certbot.compat import os
from certbot.plugins import dns_test_common
from certbot.plugins import dns_test_common_lexicon
from certbot.tests import util as test_util
from certbot_dns_nearlyfreespeech.dns_nearlyfreespeech import Authenticator

TOKEN = 'invalid-nfsn-token'
USER = 'invalid-nfsn-user'

class AuthenticatorTest(test_util.TempDirTestCase,
                        dns_test_common_lexicon.BaseLexiconAuthenticatorTest):

    def setUp(self):
        super(AuthenticatorTest, self).setUp()

        path = os.path.join(self.tempdir, 'file.ini')
        dns_test_common.write({"nearlyfreespeech_key": TOKEN}, path)
        dns_test_common.write({"nearlyfreespeech_login": USER}, path)

        self.config = mock.MagicMock(nearlyfreespeech_credentials=path,
                                     nearlyfreespeech_propagation_seconds=0)  # don't wait during tests

        self.auth = Authenticator(self.config, "nearlyfreespeech")

        self.mock_client = mock.MagicMock()
        # _get_nearlyfreespeech_client | pylint: disable=protected-access
        self.auth._get_nearlyfreespeech_client = mock.MagicMock(return_value=self.mock_client)


class NearlyFreeSpeechLexiconClientTest(unittest.TestCase, dns_test_common_lexicon.BaseLexiconClientTest):

    DOMAIN_NOT_FOUND = Exception('Domain not found')

    def setUp(self):
        from certbot_dns_nearlyfreespeech.dns_nearlyfreespeech import _NearlyFreeSpeechLexiconClient

        self.client = _NearlyFreeSpeechLexiconClient(TOKEN, USER)

        self.provider_mock = mock.MagicMock()
        self.client.provider = self.provider_mock


if __name__ == "__main__":
    unittest.main()  # pragma: no cover

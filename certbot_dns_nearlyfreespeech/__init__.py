"""
The `~certbot_dns_nearlyfreespeech.dns_nearlyfreespeech` plugin automates the process of
completing a ``dns-01`` challenge (`~acme.challenges.DNS01`) by creating, and
subsequently removing, TXT records using the NearlyFreeSpeech.net (NFSN) API.


Named Arguments
---------------

==========================================     ===================================
``--dns-nearlyfreespeech-credentials``         NFSN credentials_ INI file.
                                               (Required)
``--dns-nearlyfreespeech-propagation-seconds`` The number of seconds to wait for
                                               DNS to propagate before asking the
                                               ACME server to verify the DNS
                                               record.
==========================================     ===================================


Credentials
-----------

Use of this plugin requires a configuration file containing NearlyFreeSpeech.net API
credentials

.. code-block:: ini
   :name: credentials.ini
   :caption: Example credentials file:

   # NearlyFreeSpeech API credentials used by Certbot
   dns_nearlyfreespeech_key = XXXXXXXXXXXXXXXX
   dns_nearlyfreespeech_login = username

dns_nearlyfreespeech_key is the API key obtained using an NearlyFreeSpeech.Net support
request. dns_nearlyfreespeech_login is your NearlyFreeSpeech user name.

The path to this file can be provided interactively or using the
``--dns-nearlyfreespeech-credentials`` command-line argument. Certbot records the path
to this file for use during renewal, but does not store the file's contents.

.. caution::
   You should protect these API credentials as you would the password to your
   NearlyFreeSpeech account. Users who can read this file can use these credentials
   to issue arbitrary API calls on your behalf. Users who can cause Certbot to
   run using these credentials can complete a ``dns-01`` challenge to acquire
   new certificates or revoke existing certificates for associated domains,
   even if those domains aren't being managed by this server. Anything the API
   can do is possible with these credentials.

Certbot will emit a warning if it detects that the credentials file can be
accessed by other users on your system. The warning reads "Unsafe permissions
on credentials configuration file", followed by the path to the credentials
file. This warning will be emitted each time Certbot uses the credentials file,
including for renewal, and cannot be silenced except by addressing the issue
(e.g., by using a command like ``chmod 600`` to restrict access to the file).


Examples
--------

.. code-block:: bash
   :caption: To acquire a certificate for ``example.com``

   certbot certonly \\
     --dns-nearlyfreespeech \\
     --dns-nearlyfreespeech-credentials ~/.secrets/certbot/nearlyfreespeech.ini \\
     -d example.com

.. code-block:: bash
   :caption: To acquire a single certificate for both ``example.com`` and
             ``www.example.com``

   certbot certonly \\
     --dns-nearlyfreespeech \\
     --dns-nearlyfreespeech-credentials ~/.secrets/certbot/nearlyfreespeech.ini \\
     -d example.com \\
     -d www.example.com

.. code-block:: bash
   :caption: To acquire a certificate for ``example.com``, waiting 1000 seconds
             for DNS propagation

   certbot certonly \\
     --dns-nearlyfreespeech \\
     --dns-nearlyfreespeech-credentials ~/.secrets/certbot/nearlyfreespeech.ini \\
     --dns-nearlyfreespeech-propagation-seconds 1000 \\
     -d example.com

"""

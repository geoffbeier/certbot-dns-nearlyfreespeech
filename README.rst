NearlyFreeSpeech DNS Authenticator plugin for Certbot

This is a trivial adaptation of the Lexicon-based Linode plugin that ships with certbot.

To use it:

1. Install certbot into a virtual environment using pip.
2. Install this plugin into that same virtual environment using pip. For example::

    $ python3 -mvenv $HOME/venv-certbot
    $ source $HOME/venv-certbot/bin/activate
    $ pip install certbot
    $ pip install git+https://gitlab.com/geoffbeier/certbot-dns-nearlyfreespeech.git


3. Save your nearlyfreespeech user name and API key into an ini file::

    dns_nearlyfreespeech_key = REPLACE_WITH_YOUR_KEY
    dns_nearlyfreespeech_login = REPLACE_WITH_YOUR_USERNAME

4. Run certbot in certonly mode::

    $ certbot --work-dir=$HOME/certbot-work/work \
        --logs-dir=$HOME/certbot-work/logs \
        --config-dir=$HOME/certbot-work/config \
        certonly -d domain.example.com

    How would you like to authenticate with the ACME CA?
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    1: Obtain certs using a DNS TXT record (if you are using NearlyFreeSpeech for
    DNS). (dns-nearlyfreespeech)
    2: Spin up a temporary webserver (standalone)
    3: Place files in webroot directory (webroot)
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Select the appropriate number [1-3] then [enter] (press 'c' to cancel): 1
    Requesting a certificate for domain.example.com
    Enter email address (used for urgent renewal and security notices) (Enter 'c' to cancel): you@example.com
    [...]
    Input the path to your NearlyFreeSpeech credentials INI file (Enter 'c' to
    cancel): /home/.nearlyfreespeech_secrets.ini

